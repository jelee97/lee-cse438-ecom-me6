package com.example.cse438.studio6.activity

import android.app.ActionBar
import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.util.ListUpdateCallback
import android.util.Log
import android.util.SparseArray
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import com.example.cse438.studio6.App
import com.example.cse438.studio6.R
import com.example.cse438.studio6.enum.UserInterfaceState
import com.example.cse438.studio6.fragment.FavoritesFragment
import com.example.cse438.studio6.fragment.HomeFragment
import com.example.cse438.studio6.fragment.MyReviewsFragment
import com.example.cse438.studio6.fragment.NoConnectionFragment
import com.example.cse438.studio6.model.Product
import com.example.cse438.studio6.viewmodel.ProductViewModel
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var currentView = UserInterfaceState.HOME
    private var isNetworkConnected = false
    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_IMAGE_FROM_GALLERY = 2
    var isInitialized = false
    var observerIsSet = false

    lateinit var viewModel: ProductViewModel
    var productList = ArrayList<Product>()

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.e("FAVORITE", "Added favorite but came back here...")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.setCheckedItem(R.id.nav_home)

        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo

        // Load Fragment into View
        val fm = supportFragmentManager

        // add
        val ft = fm.beginTransaction()

        if (networkInfo == null) {
            Log.e("NETWORK", "not connected")
            ft.add(R.id.frag_placeholder, NoConnectionFragment())
        }
        if (App.firebaseAuth == null) {
            App.firebaseAuth = FirebaseAuth.getInstance()
        }

        if (App.firebaseAuth != null && App.firebaseAuth?.currentUser == null) {
            val intent = Intent(this, AccountActivity::class.java)
            startActivity(intent)
        }
        else {
            Log.e("NETWORK", "connected")
            ft.add(R.id.frag_placeholder, HomeFragment(this), "HOME_FRAG")
            this.isNetworkConnected = true
        }

        ft.commit()

        supportActionBar?.title = "Home"
        supportActionBar?.subtitle = "What's New"
    }

    override fun onStart() {
        super.onStart()

        if (!this.isInitialized) {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

            // Load Fragment into View
            val fm = supportFragmentManager

            // add
            val ft = fm.beginTransaction()

            if (networkInfo != null && App.firebaseAuth?.currentUser != null) {
                Log.e("NETWORK", "connected")
                this.isNetworkConnected = true

                if (this.currentView == UserInterfaceState.HOME) {
                    ft.replace(R.id.frag_placeholder, HomeFragment(this), "HOME_FRAG")
                }
            }
            else {
                Log.e("NETWORK", "not connected")
                ft.replace(R.id.frag_placeholder, NoConnectionFragment())
            }

            ft.commit()
            this.isInitialized = true
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    // TODO: Note the addition of this new override function and why it is needed
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if ((requestCode == REQUEST_IMAGE_CAPTURE || requestCode == REQUEST_IMAGE_FROM_GALLERY) && resultCode == Activity.RESULT_OK) {
            viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)
            this.productList = ArrayList()

            // NOTE: The line not commented out currently probably won't work unless you are using an actual device vs. an emulator
            val imgBitmap: Bitmap? = if (requestCode == REQUEST_IMAGE_CAPTURE) {
                // TODO: Load the image that was just taken. The lines below are only for testing purposes and should eventually be replaced with your code

                (getDrawable(R.drawable.test_barcode) as? BitmapDrawable)?.bitmap // Used to test a single barcode
                //(getDrawable(R.drawable.test_barcode_cluster) as? BitmapDrawable)?.bitmap // Used to test multiple barcodes
            } else {
                val returnUri = data?.data
                MediaStore.Images.Media.getBitmap(contentResolver, returnUri)
            }

            val detector = BarcodeDetector.Builder(applicationContext).setBarcodeFormats(Barcode.UPC_A).build()
            if (detector.isOperational && imgBitmap != null) {
                val frame = Frame.Builder().setBitmap(imgBitmap).build()
                val barcodes: SparseArray<Barcode> = detector.detect(frame)

                Log.e("BARCODES", barcodes.toString())

                nav_view.setCheckedItem(R.id.nav_home)

                if (barcodes.size() > 0) {
                    val barcodeArrayList = ArrayList<String>()
                    for (i in 0 until barcodes.size()) {
                        barcodeArrayList.add(barcodes.valueAt(i).displayValue)
                    }

                    if (!this.observerIsSet) {
                        val observer = Observer<ArrayList<Product>> {
                            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                                    if (p0 >= productList.size || p1 >= productList.size) {
                                        return true
                                    }
                                    return productList[p0].getId() == productList[p1].getId()
                                }

                                override fun getOldListSize(): Int {
                                    return productList.size
                                }

                                override fun getNewListSize(): Int {
                                    if (it == null) {
                                        return 0
                                    }
                                    return it.size
                                }

                                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                                    if (p0 >= productList.size || p1 >= productList.size) {
                                        return true
                                    }
                                    return (productList[p0] == productList[p1])
                                }
                            })
                            result.dispatchUpdatesTo(object : ListUpdateCallback { // TODO: Note this new kind of update dispatch that we haven't used before
                                override fun onChanged(p0: Int, p1: Int, p2: Any?) {
                                    Log.e("BARCODE_DISPATCH", "CHANGED")
                                }

                                override fun onMoved(p0: Int, p1: Int) {
                                    Log.e("BARCODE_DISPATCH", "MOVED")
                                }

                                override fun onInserted(p0: Int, p1: Int) {
                                    Log.e("BARCODE_DISPATCH", "INSERTED") // This fired, so push from here
                                    Log.e("IT_LIST", it?.toString())

                                    if (it != null && it.size > 0) {
                                        if (it.size == 1) {
                                            // Push to product detail page
                                            val intent = Intent(this@MainActivity, ProductDetailActivity::class.java)
                                            intent.putExtra("PRODUCT", it[0])
                                            startActivity(intent)
                                        }
                                        else {
                                            // Push to result list page
                                            this@MainActivity.currentView = UserInterfaceState.RESULTS

                                            // Load Fragment into View
                                            val fm = this@MainActivity.supportFragmentManager

                                            // add
                                            val ft = fm.beginTransaction()
                                            ft.replace(R.id.frag_placeholder, HomeFragment(this@MainActivity, it), "HOME_FRAG")
                                            ft.commit()

                                            supportActionBar?.title = "Search By: Barcodes"
                                            supportActionBar?.subtitle = ""
                                        }
                                    }
                                }

                                override fun onRemoved(p0: Int, p1: Int) {
                                    Log.e("BARCODE_DISPATCH", "REMOVED")

                                    if (it != null && it.size > 0) {
                                        if (it.size == 1) {
                                            // Push to product detail page
                                            val intent = Intent(this@MainActivity, ProductDetailActivity::class.java)
                                            intent.putExtra("PRODUCT", it[0])
                                            startActivity(intent)
                                        }
                                        else {
                                            // Push to result list page
                                            this@MainActivity.currentView = UserInterfaceState.RESULTS

                                            // Load Fragment into View
                                            val fm = this@MainActivity.supportFragmentManager

                                            // add
                                            val ft = fm.beginTransaction()
                                            ft.replace(R.id.frag_placeholder, HomeFragment(this@MainActivity, it), "HOME_FRAG")
                                            ft.commit()

                                            supportActionBar?.title = "Search By: Barcodes"
                                            supportActionBar?.subtitle = ""
                                        }
                                    }
                                }

                            })
                            productList = it ?: ArrayList()
                        }

                        viewModel.getProductsByUPC(barcodeArrayList).observe(this, observer)
                        observerIsSet = true
                    }
                    else {
                        viewModel.getProductsByUPC(barcodeArrayList)
                    }
                }
                else {
                    Toast.makeText(this@MainActivity, "No barcodes detected", Toast.LENGTH_SHORT).show()
                }
            }
        }
        else {
            // Set nav checked item to whatever it was before
            when (this.currentView) {
                UserInterfaceState.FAVORITES -> nav_view.setCheckedItem(R.id.nav_favorites)
                UserInterfaceState.REVIEW -> nav_view.setCheckedItem(R.id.nav_reviews)
                else -> nav_view.setCheckedItem(R.id.nav_home)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                if (this.isNetworkConnected) {
                    this.currentView = UserInterfaceState.HOME

                    // Load Fragment into View
                    val fm = supportFragmentManager

                    // add
                    val ft = fm.beginTransaction()
                    ft.replace(R.id.frag_placeholder, HomeFragment(this@MainActivity), "HOME_FRAG")
                    ft.commit()

                    supportActionBar?.title = "Home"
                    supportActionBar?.subtitle = "What's New"
                }
            }
            R.id.nav_barcode -> {
                val dialog = Dialog(this)
                dialog.setContentView(R.layout.dialog_camera_or_gallery)

                val window = dialog.window
                window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

                dialog.findViewById<Button>(R.id.gallery).setOnClickListener {
                    // TODO: Implement this click listener to load an image from the gallery

                }

                dialog.findViewById<Button>(R.id.camera).setOnClickListener {
                    // TODO: Implement this click listener to take a picture and save it to a file

                }

                dialog.findViewById<Button>(R.id.close).setOnClickListener {
                    dialog.dismiss()
                }

                dialog.show()
            }
            R.id.nav_favorites -> {
                this.currentView = UserInterfaceState.FAVORITES

                // Load Fragment into View
                val fm = supportFragmentManager

                // add
                val ft = fm.beginTransaction()
                ft.remove(fm.findFragmentById(R.id.frag_placeholder)!!)
                ft.add(R.id.frag_placeholder, FavoritesFragment(this@MainActivity), "FAVORITES_FRAG")
                ft.commit()

                supportActionBar?.title = "Favorites"
                supportActionBar?.subtitle = ""
            }
            R.id.nav_reviews -> {
                if (this.isNetworkConnected) {
                    this.currentView = UserInterfaceState.REVIEW

                    // Load Fragment into View
                    val fm = supportFragmentManager

                    // add
                    val ft = fm.beginTransaction()
                    ft.remove(fm.findFragmentById(R.id.frag_placeholder)!!)
                    ft.add(R.id.frag_placeholder, MyReviewsFragment(this@MainActivity), "REVIEWS_FRAG")
                    ft.commit()

                    supportActionBar?.title = "My Reviews"
                    supportActionBar?.subtitle = ""
                }
            }
            R.id.nav_about_us -> {
                displayDialog(R.layout.dialog_about_us)
            }
            R.id.nav_privacy_policy -> {
                displayDialog(R.layout.dialog_privacy_policy)
            }
            R.id.nav_sign_in_out -> {
                if (App.firebaseAuth?.currentUser == null) {
                    item.title = "Sign Out"
                }
                else {
                    item.title = "Sign In"

                    App.firebaseAuth?.signOut()
                }

                val intent = Intent(this, AccountActivity::class.java)
                startActivity(intent)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun displayDialog(layout: Int) {
        val dialog = Dialog(this)
        dialog.setContentView(layout)

        val window = dialog.window
        window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

        dialog.findViewById<Button>(R.id.close).setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }
}

